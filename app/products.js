const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = (db) => {
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  router.post('/', upload.single('image'), (req, res) => {
    const product = req.body;

    if (req.file) {
      product.image = req.file.filename;
    }

    db.addItem(product).then(result => {
      res.send(result);
    })
  });

  router.get('/:id', (req, res) => {
    res.send(db.getDataById(req.params.id));
  });

  return router;
};

module.exports = createRouter;